%if 0%{?fedora}
%global with_python3 1
%global _docdir_fmt %{name}
%endif

%if 0%{?rhel} && 0%{?rhel} <= 6
%{!?__python2: %global __python2 /usr/bin/python2}
%{!?python2_sitelib: %global python2_sitelib %(%{__python2} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif

%global pkgname tambo

Name:           python-%{pkgname}
Version:        0.0.9
Release:        2%{?dist}
Summary:        A command line object dispatcher
Group:          Development/Languages

License:        MIT
URL:            https://github.com/alfredodeza/tambo

Source0:        https://pypi.python.org/packages/source/t/%{pkgname}/%{pkgname}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  pytest
BuildRequires:  python2-devel
BuildRequires:  python-mock
BuildRequires:  python-setuptools
%if 0%{?with_python3}
BuildRequires:  python3-devel
BuildRequires:  python3-pytest
BuildRequires:  python3-mock
BuildRequires:  python3-setuptools
%endif # with_python3

%description
Use any argument parser you want for each sub-command. Easily manage each
command as a self-contained application.

%if 0%{?with_python3}
%package -n python3-tambo
Summary:        A command line object dispatcher
Group:          Development/Languages
Requires:       python3

%description -n python3-tambo
Use any argument parser you want for each sub-command. Easily manage each
command as a self-contained application.
%endif # with_python3

%prep
%setup -q -n %{pkgname}-%{version}

%if 0%{?with_python3}
cp -a . %{py3dir}
%endif # with_python3

%build
%{__python2} setup.py build

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py build
popd
%endif # with_python3

%install
%{__python2} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
popd
%endif # with_python3


%check
export PYTHONPATH=$(pwd)

py.test-%{python_version} -v tambo/tests

%if 0%{?with_python3}
pushd %{py3dir}
py.test-%{python3_version} -v tambo/tests
popd
%endif # with_python3

%files
%{!?_licensedir:%global license %%doc}
%doc README.rst
%license LICENSE
%{python2_sitelib}/*

%if 0%{?with_python3}
%files -n python3-tambo
%{python3_sitelib}/*
%license LICENSE
%doc README.rst
%endif # with_python3


%changelog
* Sat Jun 27 2015 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.0.9-2
- set _docdir_fmt to get the same license and doc directories for python- and
  python3- subpackages (rhbz#1208665)

* Thu Apr 02 2015 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.0.9-1
- Initial package
